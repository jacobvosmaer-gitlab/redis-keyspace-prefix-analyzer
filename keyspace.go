package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
)

const progname = "redis-keyspace-prefix-analyzer"

func main() {
	jsonMode := flag.Bool("json", false, "input is JSON in with 'key' and 'weight' fields")
	flag.Parse()
	args := flag.Args()

	log.Print(args)
	if len(args) < 2 {
		fatal(fmt.Sprintf("usage: %s MAX_DEPTH MIN_WEIGHT [SCRUB_RULE...]", progname))
	}

	maxDepth, err := strconv.Atoi(args[0])
	noErr(err)
	minWeight, err := strconv.ParseInt(args[1], 10, 64)
	noErr(err)
	extraRules, err := parseScrubRules(args[2:])
	noErr(err)

	var scanFunc func() (*Record, error)

	if *jsonMode {
		decoder := json.NewDecoder(os.Stdin)
		scanFunc = func() (*Record, error) {
			rec := &Record{}
			return rec, decoder.Decode(rec)
		}
	} else {
		r := bufio.NewReader(os.Stdin)
		scanFunc = func() (*Record, error) {
			line, err := r.ReadString('\n')
			if err != nil {
				return nil, err
			}
			return &Record{Key: line[:len(line)-1], Weight: 1}, nil
		}
	}

	noErr(scan(scanFunc, maxDepth, minWeight, extraRules))
}

type Record struct {
	Key    string `json:"key"`
	Weight int64  `json:"weight"`
}

func noErr(err error) {
	if err != nil {
		fatal(err)
	}
}

func fatal(msg interface{}) {
	fmt.Fprintln(os.Stderr, msg)
	os.Exit(1)
}

func parseScrubRules(in []string) ([]scrubRule, error) {
	result := make([]scrubRule, len(in))

	for i, raw := range in {
		result[i].placeholder = "X"

		if !strings.Contains(raw, "(") {
			raw = raw + "(.*)"
		}

		var err error
		result[i].Regexp, err = regexp.Compile("^" + raw)
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

func scan(scanFunc func() (*Record, error), maxDepth int, minWeight int64, extraRules []scrubRule) error {
	const minDepth = 4
	if maxDepth < minDepth {
		maxDepth = minDepth
	}

	scrubRules := append(extraRules, defaultScrubRules...)

	records := make(chan *Record)
	scanErr := make(chan error)
	n := 0
	log.Print("starting scan")

	go func() {
		var err error
		for {
			var rec *Record
			rec, err = scanFunc()
			if err != nil {
				break
			}
			records <- rec
			n++
		}
		if err == io.EOF {
			err = nil
		}
		scanErr <- err
	}()

	scrubbedRecords := make(chan *Record)
	wg := &sync.WaitGroup{}
	const scrubWorkers = 8
	for i := 0; i < scrubWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for rec := range records {
				if len(rec.Key) > maxDepth {
					rec.Key = rec.Key[:maxDepth]
				}
				rec.Key = string(scrub([]byte(rec.Key), scrubRules))
				scrubbedRecords <- rec
			}
		}()
	}

	weights := make(map[string]int64)
	countDone := make(chan struct{})
	go func() {
		for rec := range scrubbedRecords {
			for i := minDepth; i <= len(rec.Key); i++ {
				weights[rec.Key[:i]] += rec.Weight
			}
		}
		close(countDone)
	}()

	if err := <-scanErr; err != nil {
		return err
	}
	log.Printf("scanned %d keys", n)

	close(records)
	wg.Wait()

	close(scrubbedRecords)
	<-countDone
	log.Printf("counted %d distinct prefixes", len(weights))

	garbage := regexp.MustCompile(`:[^:][^:]?$`)
	var interestingKeys []string
	for k, v := range weights {
		if v < minWeight || garbage.MatchString(k) {
			continue
		}

		interestingKeys = append(interestingKeys, k)
	}

	sort.Strings(interestingKeys)

	var deduplicatedKeys []string
	for i, k := range interestingKeys {
		if i < len(interestingKeys)-1 {
			// If we have ("fooba", 6) and ("foobar", 6), then don't bother printing
			// "fooba".
			next := interestingKeys[i+1]
			if strings.HasPrefix(next, k) && weights[next] == weights[k] {
				continue
			}
		}

		deduplicatedKeys = append(deduplicatedKeys, k)
	}

	for _, k := range deduplicatedKeys {
		fmt.Printf("%15d\t%s\n", weights[k], k)
	}

	return nil
}

type scrubRule struct {
	*regexp.Regexp
	// The first submatch of Regexp will be replaced by placeholder. Make
	// sure that the placeholder is short enough to fit in the space of the
	// smallest submatch. If you match on "3 or more numbers', then "NUM" is
	// OK, but "NUMBER" is too big.
	placeholder string
}

var defaultScrubRules = []scrubRule{
	// I gave up on IP addresses because we'd have to support IPv6, and IPv6 is regex-unfriendly. Better use custom prefix-based regexes
	//
	// 	{
	// 		Regexp:      regexp.MustCompile(`[^0-9.]([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})([^0-9.]|$)`),
	// 		placeholder: "IP4ADDR",
	// 	},
	// 	{
	// 		Regexp:      regexp.MustCompile(`[^0-9a-f]([0-9a-f]{1,4}:[0-9a-f]{1,4}:[0-9a-f]{1,4}:[0-9a-f]{1,4}:[0-9a-f]{1,4}:[0-9a-f]{1,4}:[0-9a-f]{1,4}:[0-9a-f]{1,4})([^0-9a-f:]|$)`),
	// 		placeholder: "IP6ADDR",
	// 	},
	{
		Regexp:      regexp.MustCompile(`[^0-9a-f]([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})([^0-9a-f]|$)`),
		placeholder: "UUID",
	},
	{
		Regexp:      regexp.MustCompile(`[^a-f0-9]([a-f0-9]{10,})`),
		placeholder: "HEX",
	},
	{
		Regexp:      regexp.MustCompile(`[^0-9]([0-9]{4,})`),
		placeholder: "NUM",
	},
}

func scrub(key []byte, rules []scrubRule) []byte {
	for _, rule := range rules {
		allMatches := rule.FindAllSubmatchIndex(key, -1)

		// Work right-to-left through the submatches because we are deleting
		// bytes which invalidates indices to the right of the deleted section.
		for i := len(allMatches) - 1; i >= 0; i-- {
			matches := allMatches[i]

			// matches[:2] is the whole match; matches[2:4] is the first submatch
			left, right := matches[2], matches[3]

			replace := rule.placeholder
			if n := right - left; len(replace) > n {
				replace = replace[:n]
			}

			// key[left:right] is the submatch we want to replace
			cursor := left
			copy(key[cursor:], []byte(replace))
			cursor += len(replace)

			copy(key[cursor:], key[right:])
			cursor += len(key[right:])

			key = key[:cursor]
		}
	}

	return key
}
